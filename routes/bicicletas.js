var express = require('express');
var router = express.Router();
var bicicletaController = require('../controllers/bicicleta');

router.get('/', bicicletaController.bicicleta_list);
router.get('/create', bicicletaController.bicicletaCreate_get)
router.post('/create', bicicletaController.bicicletaCreate_post)
router.get('/:id/update', bicicletaController.bicicletaUpdate_get)
router.post('/:id/update', bicicletaController.bicicletaUpdate_post)
router.post('/:id/delete', bicicletaController.bicicleta_delete_post)
module.exports = router;