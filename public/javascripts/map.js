var map = L.map('main_map').setView([15.7777498,-96.1421051], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
    attribute: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map)

$.ajax({
    dataType:"json",
    url:"api/bicicletas",
    success: function(result){
        console.log(result)
        result.bicicletas.forEach( 
            bici => L.marker(bici.ubicacion, {title: bici.id}).addTo(map)
        )
    }
})