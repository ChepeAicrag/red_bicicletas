const { all } = require("../app");

// Forzozamente con function
var Bicicleta = function(id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
};

Bicicleta.prototype.toString = () => 'id: ' + this.id + ' | color: ' + this.color;

Bicicleta.allBicis = [];

Bicicleta.add = (aBici) => Bicicleta.allBicis.push(aBici);

Bicicleta.findById = (aBiciId) => Bicicleta.allBicis.find( x => x.id == aBiciId) ;


Bicicleta.removeById = (id) => 
    Bicicleta.allBicis.forEach((x, index) => 
        x.id == id ? Bicicleta.allBicis.splice(index, 1): new Error('Elemento no removido')
        );
var a = new Bicicleta(1, 'rojo', 'urbana',   [15.7777498,-96.14215551]);
var b = new Bicicleta(2, 'blanca', 'urbana', [15.7777498,-96.14421151]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;