# PROYECTO RED DE BICICLETAS
Practica del curso de 'Desarrollo del lado del servidor con Nodejs & Express & Mongodb'.
Pasos para ejecutar el proyecto.

### REQUISITOS
*   [nodejs](https://nodejs.dev/) 
*   [postman](postman.com) (opcional)

### 1. Clonar el repositorio. 

### 2. Instalar las dependencias del proyecto.
```
npm install 
```

### 3. Ejecutar el comando.
```
node run devstart
```

### 4. Por último, abrir en el navegador.
* [localhost:8080](http://localhost:8080/)
